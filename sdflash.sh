#! /bin/bash


# usage sudo bash ./sdflash.sh <drive>
# example: sudo bash ./sdflash.sh /dev/sdb
# example: sudo bash ./sdflash.sh /dev/mmcblk0

set -e
TARGETPATH="$1"

if [ "$UID" -ne 0 ]
  then echo "Flash Error: Please run as root"
  exit
fi
if [ ! -w "${TARGETPATH}" ]
  then echo "error: can't access target \"${TARGETPATH}\"."
  exit 1
fi
if [ ! -b "${TARGETPATH}" ]
  then echo "warning: target is not a block device."
fi


imageloc=.


echo Flashing SD - DO NOT REMOVE!


dest_device=${TARGETPATH} \
	source_image_part1=${imageloc}/bbb.01.07.14-part1 \
	source_image_part2=${imageloc}/bbb.03.20.14-part2 \
	bash ${imageloc}/flash.sh



echo You are free to go now.
