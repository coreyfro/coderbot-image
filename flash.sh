#!/bin/bash
set -e

test "${dest_device}" == "/dev/mmcblk0" && dev_enum="p"
usage="Usage: dest_device=<dev or raw disk img> source_image_part1=<part1.img> source_image_part2=<part2.img> sh flash.sh"

DIR=$PWD


check_defines () {
    if [ ! -r "${dest_device}" ] ; then
        echo "flash.sh: Error: can't read dest_device \"${dest_device}\""
		echo "$usage"
		exit 1
    fi

    if [ ! -r "${source_image_part1}" ] ; then
        echo "flash.sh: Error: can't read source_image_part1 \"${source_image_part1}\""
		echo "$usage"
        exit 1
    fi

    if [ ! -r "${source_image_part2}" ] ; then
        echo "flash.sh: Error: can't read source_image_part2 \"${source_image_part2}\""
		echo "$usage"
        exit 1
    fi

	if [ ! "${force}" ] ; then
		if [ "0`sfdisk -s ${dest_device} 2> /dev/null`0" -gt "377619200"  ] ; then

	        echo "flash.sh: Error: Device is large and \"force\" is undefined"
			echo "$usage"

            exit 1
		fi
    fi
}

check_images () {

	file "${source_image_part1}" | grep -q x86 &> /dev/null
	if [ "$?" != "0" ] ; then
		echo "FS Improper : ${source_image_part1} is not a fat partition or is not bootable"
		exit 1
	fi
	if [ $(blkid -o value -s TYPE "${source_image_part2}") != "ext4" ]; then
		echo "FS Improper : ${source_image_part2} is not an ext4 partition"
		exit 1
	fi

}

clean_mount () {
	sync
	mounted=$(mount | cut -d ' ' -f 1)
	for f in "${dest_device}"*; do
	    if mount | cut -d ' ' -f 1 | grep -q "^${f}$"; then
			umount $f
	    fi
	done

	case `file -b "${dest_device}"` in

		*block* )
			Part1="${dest_device}${dev_enum}"1
			Part2="${dest_device}${dev_enum}"2
			Part3="${dest_device}${dev_enum}"3

			;;
		data )
			kpartx -d "${dest_device}"
			sync
			;;
		*x86* )
			kpartx -d "${dest_device}"
			sync
			declare -a Parts=(`kpartx -av "${dest_device}" | cut -d" " -f3`)

			b=1
			for i in "${Parts[@]}"
			do
				export Part$(( b++ ))=/dev/mapper/"$i"
			done
			;;
		*cannot* ) echo no loop

			if [ ! $"size" ] ; then
				size="7761920"
			fi
			touch "${dest_device}"
			kpartx -d "${dest_device}"
			truncate -s "$size" "${dest_device}"
			sync

			;;
		* ) echo "Unrecognized device or image.  If an image file exists, please delete it and try again."
			exit 1
			;;
	esac
}

#set -x
echo "==============================================================================="
echo -e "\\nFlashing ${dest_device}\\nPlease wait for prompt before removing SD card\\n"


check_defines
check_images
clean_mount

echo "partitioning"

./scripts/partition.py "$dest_device" "$source_image_part1" "$source_image_part2"

echo "done partitioning"

clean_mount

partprobe ${dest_device}

clean_mount
echo -e "Formatting $Part3"
mkfs.ext4 -qF "$Part3" -L "Prints" -E root_owner=1000:1000

clean_mount

pv "$source_image_part1" | dd of="$Part1" bs=4M status=none

clean_mount

echo -e "Writing complete\\n\\nWriting $Part2"
pv "$source_image_part2" | dd of="$Part2" bs=4M status=none

clean_mount

echo "Image process complete"
