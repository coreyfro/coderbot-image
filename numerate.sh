#! /bin/bash
#
# numerate.sh creates and updates identification files for the Series
# 1 3D printer image files it is intended to be run on a system with
# an sdcard image containing a BBB filesystem, mounted for editing
#
# requires sudo to work
# usage numerate.sh <targetpath> <serial#>
# example numerate.sh /dev/sdb number/name
# example numerate.sh /dev/mmcblk0 number/name

set -e

# customer login
username="ubuntu"

octoprint_git_rev="v1.1.1a3"
ubuntu_release="precise"
serialbase="series1-"

git_version=$(git describe --dirty)
if [[ $git_version = *-dirty ]]; then
	echo "Code contains changes that aren't checked in:"
	git status --short
	exit 1
fi
echo "image builder version: $git_version"

overwrite=0

while :; do
    case $1 in
		--overwrite)
			overwrite=1
			;;
        --)						# End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *) # Default case: If no more options then break out of the loop.
            break
    esac

    command shift
done

targetpath="$1"
targetname="$2"
test "${targetpath}" == "/dev/mmcblk0" && dev_enum="p"
if [ ! ${targetpath} ]
  then echo "no target path supplied"
  exit
fi


if [ "$UID" -ne 0 ]
then echo "Numerate Error: Please run as root"
	exit
fi

mkdir -p /mnt/rootfs
target=/mnt/rootfs
if ! mount  ${targetpath}${dev_enum}2 /mnt/rootfs; then
    echo "Try 'sudo umount ${targetpath}${dev_enum}2'"; exit 1
fi

expected_old_hostname="series1-(unconfigured|image|1000)"
old_hostname=$(cat "$target/etc/hostname")
if [[ ! $old_hostname =~ $expected_old_hostname ]]; then
	echo "Based on /etc/hostname (${old_hostname}), this doesn't look like an unconfigured image."
	if [ $overwrite = 1 ]; then
		echo "Overwriting"
		expected_old_hostname=$old_hostname
	else
		echo tough luck
		umount $target
		exit 1
	fi
fi

if [ ${targetname} ]; then
  serial="${targetname}"
else
  echo Please enter the serial number or name of the machine to be enumerated
  read serial
  serial="${serial}"
fi
read -r -p "Is ${serialbase}${serial} the correct name of the machine? [y/N]" response
case $response in 
  [yY][eE][sS]|[yY])
    echo writing now -- please wait
    ;;
  *)
    echo please try again
    umount $target
    exit 1
    ;;
esac

echo "${serial}" > $target/etc/unit

home="${target}/home/${username}"

# record the OS version
cp "files/type-a-release.base" files/type-a-release
echo "TYPE_A_VERSION=\"$git_version\"" >> files/type-a-release
install --owner root --group root --mode="a=r" files/type-a-release $target/etc/

# add octoprint config
cp -r "files/series 1" "${home}/.series 1"

# remove any memory of previous wifi adaptors
rm -f "${target}/etc/udev/rules.d/70-persistent-net.rules"
# don't automatically generate names for new net devices
rm -f $target/lib/udev/rules.d/75-persistent-net-generator.rules
# rename ralink devices wlan0
cp files/70-persistent-net.rules $target/etc/udev/rules.d/.
cp files/forgetsafe.conf $target/etc/init/.
echo "manual" > $target/etc/init/failsafe.override
# set dhcp timeouts
install files/dhclient.conf $target/etc/dhcp/ --owner=root --mode='-rw-r--r--'
# network settings files
chown :dialout "$target/etc/network/"
chmod g+w "$target/etc/network/"
install files/interfaces "$target/etc/network/" \
    --owner=root --group=dialout --mode="ug=rw,o=-"
install files/interfaces.template "$target/etc/network/" \
    --group=dialout --mode="ug=r,o=-"

# make sure we have unique ssh host keys
rm $target/etc/ssh/*key*
ssh-keygen -t dsa -N "" -f $target/etc/ssh/ssh_host_dsa_key > /dev/null
ssh-keygen -t rsa -N "" -f $target/etc/ssh/ssh_host_rsa_key > /dev/null
ssh-keygen -t ecdsa -N "" -f $target/etc/ssh/ssh_host_ecdsa_key > /dev/null

# set the hostname based on the hardware serial number
hostname="${serialbase}${serial}"
echo "$hostname" > $target/etc/hostname
cp $target/etc/hosts $target/etc/hosts.old
sed -r "s/$expected_old_hostname/$hostname/g" \
	$target/etc/hosts.old > $target/etc/hosts
if [ ! $? ]; then
	echo "Failed to edit hosts file." 1>&2
	umount $target
	exit 1
fi
rm $target/etc/hosts.old

# shallow clone octoprint from local git repo and then set remote
octoprint_home="${home}/octoprint-type-a"
octoprint_git_remote="https://bitbucket.org/typeamachines/octoprint-type-a.git"
octoprint_git_cache="/var/tmp/${USER}_octoprint.git"
if [ ! -d "$octoprint_git_cache" ]; then
	git clone --bare "$octoprint_git_remote" "$octoprint_git_cache"
else
	git --git-dir="$octoprint_git_cache" fetch # update cache
fi

rm -rf "$octoprint_home"
git clone --no-hardlinks --depth=1 --branch "$octoprint_git_rev" \
	-c advice.detachedHead=false \
	-- "file://${octoprint_git_cache}" "$octoprint_home"
git --git-dir="${octoprint_home}/.git" remote remove origin
git --git-dir="${octoprint_home}/.git" remote add origin \
	"$octoprint_git_remote"
chown -R "${username}:${username}" "$octoprint_home"

source $target/etc/type-a-release
if [ "$TYPE_A_VERSION" != "$git_version" ]; then
	echo "Write failed!"
	umount $target
	exit 1
fi

umount $target

echo "$serial" >> ~/serials
echo "numerate: done."
