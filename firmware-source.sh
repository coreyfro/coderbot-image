#!/bin/bash
source_dirs="/nonfree-firm/firmware-nonfree/atheros/ar3k /nonfree-firm/firmware-nonfree/atheros/ath6k /nonfree-firm/firmware-nonfree/brcm80211/brcm /nonfree-firm/firmware-nonfree/libertas /nonfree-firm/firmware-nonfree/libertas/mrvl /nonfree-firm/firmware-nonfree/libertas/mwl8k /nonfree-firm/firmware-nonfree/realtek/RTL8192E /nonfree-firm/firmware-nonfree/realtek/RTL8192SU /nonfree-firm/firmware-nonfree/realtek/rtl_nic /nonfree-firm/firmware-nonfree/realtek/rtlwifi"

source_files="/nonfree-firm/firmware-nonfree/atheros/ar5523.bin /nonfree-firm/firmware-nonfree/atheros/ar7010_1_1.fw /nonfree-firm/firmware-nonfree/atheros/ar7010.fw /nonfree-firm/firmware-nonfree/atheros/ar9170.fw /nonfree-firm/firmware-nonfree/atheros/ar9271.fw /nonfree-firm/firmware-nonfree/atheros/ath3k-1.fw /nonfree-firm/firmware-nonfree/atheros/htc_7010.fw /nonfree-firm/firmware-nonfree/atheros/htc_9271.fw /nonfree-firm/firmware-nonfree/ralink/mt7601u.bin /nonfree-firm/firmware-nonfree/ralink/rt2561.bin /nonfree-firm/firmware-nonfree/ralink/rt2561s.bin /nonfree-firm/firmware-nonfree/ralink/rt2661.bin /nonfree-firm/firmware-nonfree/ralink/rt2860.bin /nonfree-firm/firmware-nonfree/ralink/rt2870.bin /nonfree-firm/firmware-nonfree/ralink/rt3070.bin /nonfree-firm/firmware-nonfree/ralink/rt3071.bin /nonfree-firm/firmware-nonfree/ralink/rt3090.bin /nonfree-firm/firmware-nonfree/ralink/rt3290.bin /nonfree-firm/firmware-nonfree/ralink/rt73.bin"


mkdir -p "images/$ImageName/lib/firmware/"

# copy support binaries
for i in $source_dirs
do
        cp -r "$FIRMWARE/$i" "images/$ImageName/lib/firmware/"
done

for i in $source_files
do
        cp "$FIRMWARE/$i" "images/$ImageName/lib/firmware/"
done

ln -s images/$ImageName/lib/firmware/libertas/cf8385.bin images/$ImageName/lib/firmware/libertas_cs.fw
ln -s images/$ImageName/lib/firmware/libertas/cf8385_helper.bin images/$ImageName/lib/firmware/libertas_cs_helper.fw
ln -s images/$ImageName/lib/firmware/rt2870.bin images/$ImageName/lib/firmware/rt3070.bin
ln -s images/$ImageName/lib/firmware/rt2860.bin images/$ImageName/lib/firmware/rt3090.bin
ln -s images/$ImageName/lib/firmware/libertas/sd8385.bin images/$ImageName/lib/firmware/sd8385.bin
ln -s images/$ImageName/lib/firmware/libertas/sd8385_helper.bin images/$ImageName/lib/firmware/sd8385_helper.bin
ln -s images/$ImageName/lib/firmware/libertas/sd8686_v8.bin images/$ImageName/lib/firmware/sd8686.bin
ln -s images/$ImageName/lib/firmware/libertas/sd8686_v8_helper.bin images/$ImageName/lib/firmware/sd8686_helper.bin
ln -s images/$ImageName/lib/firmware/mrvl/sd8688.bin images/$ImageName/lib/firmware/sd8688.bin
ln -s images/$ImageName/lib/firmware/mrvl/sd8688_helper.bin images/$ImageName/lib/firmware/sd8688_helper.bin
ln -s images/$ImageName/lib/firmware/libertas/usb8388_v5.bin images/$ImageName/lib/firmware/usb8388.bin
