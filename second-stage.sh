#!/bin/bash
set -x

groups="adm,dialout,audio,video"
user=pi
pass=coderbot

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true LC_ALL=C LANGUAGE=C LANG=C
ln -s /usr/lib/insserv/insserv /sbin/insserv
/var/lib/dpkg/info/dash.preinst install
dpkg --configure -a ; rm -r /var/run/* ; dpkg --configure -a ; rm -r /var/run/* ; dpkg --configure -a
useradd -mU "${user}" -s /bin/bash --groups ${groups}
echo ${user}:${pass} | chpasswd
mkdir -p "/home/${user}/build/"
chown -R "$user":"$user" /home/"$user"

rm configscript.sh

exit
