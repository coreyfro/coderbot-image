Coderbot Raspbian Image Builder
=============================

This Multistrap system builds linux root filesystem images (currently) based on Debian for use with the Raspberry Pi for Coderbot.org programable education robots.

At the time of writing we use Raspbian Jessie and the system installs without firmware

Building Images with Multistrap
===============================

This version assumes nothing and is not, yet, user friendlu.

Use apt-get (or aptitude or the package manager for your system as you like) to add the following packages
multistrap (currently 2.2.0ubuntu1)
binfmt-support
qemu
qemu-user-static
pv
python-parted

Process to build an image with multistrap
========================================

Note that the steps can require environment variables to be declared before execution

1. Build an image with the script called `coderbot-image.sh`. At the command line prompt of your build system, type the following command stanza:

sudo dist=jessie bash coderbot-image.sh

2. Image writing process is manual and not, yet, documented

