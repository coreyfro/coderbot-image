#!/bin/bash
set -x
DIR=$PWD
FIRMWARE="$DIR/firmware/"
source $FIRMWARE/firmware-source.sh
hostname=coderbot

# Tests run for sanity

#  ${dist} is passed from the environment that called this script in the first place
check_defines () {
	if [ ! "${dist}" ] ; then
		echo "bbb-image.sh: Error: dist undefined"
		exit 1
	fi
	}

check_multistrap () {
	if [ ! "`which multistrap`" ] ; then
		echo "bbb-image.sh: Error: multistrap not installed"
		exit 1
	fi
	}

check_root () {
	if [ "$UID" -ne "0" ] ; then
		echo "bbb-image.sh Error: Please run as root"
		exit 1
	fi
	}

check_git_free () {
	if [ -e "$FIRMWARE/rpi-firm/firmware/" ] ; then
		echo "bbb-image.sh Updating: Pulling RPi firmware files"
		git -C $FIRMWARE/rpi-firm/firmware/ pull

	else
		echo "bbb-image.sh Warning: Must fetch RPi firmware files"
		mkdir -p $FIRMWARE/rpi-firm/
		git -C $FIRMWARE/rpi-firm/ clone https://github.com/raspberrypi/firmware.git
	fi
	}

check_git_kernel () {
	if [ -e "$FIRMWARE/kern-firm/linux-firmware/" ] ; then
		echo "bbb-image.sh Updating: Pulling Kernel firmware files"
		git -C $FIRMWARE/kern-firm/linux-firmware pull

	else
		echo "bbb-image.sh Warning: Must fetch Kernel firmware files"
		mkdir -p $FIRMWARE/kern-firm/
		git -C $FIRMWARE/kern-firm clone http://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git
	fi
	}

check_git_nonfree () {
	if [ -e "$FIRMWARE/nonfree-firm/firmware-nonfree/" ] ; then
		echo "bbb-image.sh Updating: Pulling RPi-nonfree firmware files"
		git -C $FIRMWARE/nonfree-firm/firmware-nonfree/ pull

	else
		echo "bbb-image.sh Warning: Must fetch RPi-nonfree firmware files"
		mkdir -p $FIRMWARE/nonfree-firm/
		git -C $FIRMWARE/nonfree-firm clone https://github.com/RPi-Distro/firmware-nonfree.git
	fi
	}

check_git_coderbot () {
	if [ -e "$FIRMWARE/coderbot/coderbot" ] ; then
		echo "bbb-image.sh Updating: Pulling Coderbot files"
		git -C $FIRMWARE/coderbot/coderbot pull

	else
		echo "bbb-image.sh Warning: Must fetch Coderbot files"
		mkdir -p $FIRMWARE/coderbot/
		git -C $FIRMWARE/coderbot clone https://github.com/previ/coderbot.git
	fi
	}

check_git_opencv () {
	if [ -e "$FIRMWARE/opencv/opencv_contrib/.git/" -a -e "$FIRMWARE/opencv/opencv/.git/" ] ; then
		echo "bbb-image.sh Updating: Pulling Opencv files"
		git -C $FIRMWARE/opencv/opencv/ pull https://github.com/Itseez/opencv.git 3.2.0
		git -C $FIRMWARE/opencv/opencv_contrib/ pull https://github.com/Itseez/opencv_contrib.git 3.2.0

	else
		echo "bbb-image.sh Warning: Must fetch Opencv files"
		mkdir -p $FIRMWARE/opencv/
		git -C $FIRMWARE/opencv clone https://github.com/Itseez/opencv.git
		mkdir -p $FIRMWARE/opencv/opencv_contrib/
		git -C $FIRMWARE/opencv clone https://github.com/Itseez/opencv_contrib.git
		git -C $FIRMWARE/opencv/opencv checkout 3.2.0
		git -C $FIRMWARE/opencv/opencv_contrib checkout 3.2.0
	fi
	}


check_pigpio () {
	if [ -e "$FIRMWARE/pigpio.zip" ] ; then
		echo "bbb-image.sh Status : Detected PIGPIO.ZIP"
	else
		echo "bbb-image.sh Warning: Downloading PIGPIO.ZIP"
		wget http://abyz.co.uk/rpi/pigpio/pigpio.zip -O $FIRMWARE/pigpio.zip
	fi
	}









increment_version ()
{
  declare -a part=( ${1//\./ } )
  declare    new
  declare -i carry=1

  for (( CNTR=${#part[@]}-1; CNTR>=0; CNTR-=1 )); do
    len=${#part[CNTR]}
    new=$((part[CNTR]+carry))
    [ ${#new} -gt $len ] && carry=1 || carry=0
    [ $CNTR -gt 0 ] && part[CNTR]=${new: -len} || part[CNTR]=${new}
  done
  new="${part[*]}"
  echo -e "${new// /.}"
} 

check_root
check_defines
check_multistrap
check_git_free
check_git_kernel
check_git_nonfree
check_git_opencv
check_git_coderbot
check_pigpio

# End of tests

# Build
build="`cat files/local-version`"
build="`increment_version $build`"
echo "$build" > files/local-version

# Timestamp
NIXDate="`date +%Y.%m.%d`"
NIXTime="$NIXDate-`date +%H.%M`"
ImageName="$dist-$NIXTime"
echo -e "\\n\\nBuild version $build - Build time $NIXTime\\n\\n"

# Issue Footer
source files/os-release.$dist
# Footer Example
#	Ubuntu 12.04.2 LTS \n \l
#
#	Firmware 1.8, build date 2014.05.11

Issue_Footer="$VERSION \\\\n \\\\l\\n\\nFirmware $build, build date $NIXDate"

#echo -e "$Issue_Footer"


# initialize image
mkdir -p "images/$ImageName" "source/$dist"
# truncate creates a file argument when it does not already exist, the -s switch allows one to set SIZE
# in bytes, this is 1750MB
#truncate -s 1835008000 "images/$ImageName-root.image"
truncate -s 7000000000 "images/$ImageName-root.image"
truncate -s 50000000 "images/$ImageName-boot.image"
# mkfs.ext4 is a tool that simply creates an ext4 filesystem
mkfs.ext4 -F "images/$ImageName-root.image"
# mkfs.vfat is a tool that simply creates an vfat filesystem
mkfs.vfat "images/$ImageName-boot.image"
# mount the blank image as a loop device to the images directory
mount -o loop "images/$ImageName-root.image" "images/$ImageName"
# create archives and uboot directory tree on the image 
mkdir -p "images/$ImageName/var/cache/apt/archives/" "images/$ImageName/boot/" "images/$ImageName/lib/modules/" "images/$ImageName/lib/firmware/"

# mount the blank image as a loop device to the images directory
mount -o loop "images/$ImageName-boot.image" "images/$ImageName/boot"
# Link source files to image so they aren't downloaded again
mount -o bind "source/$dist" "images/$ImageName/var/cache/apt/archives/"

# copy support binaries

rsync -ruv "$FIRMWARE/rpi-firm/firmware/hardfp/opt" "images/$ImageName/"
rsync -ruv "$FIRMWARE/rpi-firm/firmware/modules" "images/$ImageName/lib/"

# And use multistrap to build out the mounted image 
{ /usr/sbin/multistrap -f $dist.conf --dir "images/$ImageName" 2>&1; } 2>&1 | tee $ImageName.log


#POST-STRAP

# copy custom files to image
cp -a files/resolv.conf files/fstab files/hosts "images/$ImageName/etc/"
cp -a $(which qemu-arm-static) "images/$ImageName/usr/bin"
echo "coderbot" > "images/$ImageName/etc/hostname"

# configure
echo "America/Los_Angeles" > "images/$ImageName/etc/timezone"
echo 'pi ALL=(ALL:ALL) ALL' >> "images/$ImageName/etc/sudoers"

# start chroot
mount -t proc proc "images/$ImageName/proc"
mount -o bind /dev "images/$ImageName/dev"
mount -o bind /sys "images/$ImageName/sys"
cp second-stage.sh "images/$ImageName/tmp"
chroot "images/$ImageName" /bin/bash /tmp/second-stage.sh 2>&1 | tee -a $ImageName.log

# Cleanup
rm "images/$ImageName/tmp/second-stage.sh"

# Post config setup
mkdir -p images/$ImageName/home/pi/coderbot images/$ImageName/home/pi/build/opencv images/$ImageName/home/pi/build/opencv_contrib
cp -r $FIRMWARE/coderbot/coderbot/* images/$ImageName/home/pi/coderbot/.
cp -r $FIRMWARE/opencv/opencv/* images/$ImageName/home/pi/build/opencv/.
cp -r $FIRMWARE/opencv/opencv_contrib/* images/$ImageName/home/pi/build/opencv_contrib/.
unzip $FIRMWARE/pigpio.zip -d images/$ImageName/home/pi/build/
rsync -ruv "$FIRMWARE/rpi-firm/firmware/boot" "images/$ImageName/"

source firmware-source.sh

cp files/issue "images/$ImageName/boot/."
wget https://raw.githubusercontent.com/RPi-Distro/pi-gen/dev/stage1/00-boot-files/files/config.txt -qO "images/$ImageName/boot/config.txt"
echo "enable_uart=1" >> "images/$ImageName/boot/config.txt"
wget https://raw.githubusercontent.com/RPi-Distro/pi-gen/dev/stage1/00-boot-files/files/cmdline.txt -qO- | sed 's/ROOTDEV/\/dev\/mmcblk0p2/g' > "images/$ImageName/boot/cmdline.txt"

wget https://raw.githubusercontent.com/CoderBotOrg/coderbot/master/scripts/pigpiod -O images/$ImageName/etc/init.d/pigpiod
cp -r $FIRMWARE/coderbot/coderbot/scripts/coderbot images/$ImageName/etc/init.d/coderbot
chmod 755 images/$ImageName/etc/init.d/pigpiod
chmod 755 images/$ImageName/etc/init.d/coderbot


chown -R 1000 images/$ImageName/home/pi/

cp third-stage.sh "images/$ImageName/tmp"
chroot "images/$ImageName" /bin/bash /tmp/third-stage.sh 2>&1 | tee -a $ImageName.log

# Cleanup
rm "images/$ImageName/tmp/third-stage.sh"


# Issue and MOTD timestamps and build info
mkdir -p "images/$ImageName/etc/update-motd.d/"
cp "files/99-footer" "images/$ImageName/etc/update-motd.d/"
cp "files/issue" "files/motd.tail" "images/$ImageName/etc/."
#cp "files/os-release.$dist" "images/$ImageName/etc/os-release"
#echo "BUILD_ID=\"$build\"" >> "images/$ImageName/etc/os-release"
echo -e "$Issue_Footer" |tail -n1 >> images/$ImageName/etc/motd.tail
echo -e "$Issue_Footer" >> images/$ImageName/etc/issue
cp images/$ImageName/etc/issue images/$ImageName/etc/issue.net

# copy networking configs
cp "files/wpa_supplicant.conf" "images/$ImageName/etc/wpa_supplicant/."
cp "files/interfaces" "images/$ImageName/etc/network/."
cp "files/hostapd.conf" "images/$ImageName/etc/hostapd/."
echo 'DAEMON_CONF="/etc/hostapd/hostapd.conf"' > "images/$ImageName/etc/default/hostapd"
cp "files/dnsmasq.conf" "images/$ImageName/etc/dnsmasq.conf"

# config sound
cp "files/asound.conf" "images/$ImageName/etc/."
cp "files/alsa-base.conf" "images/$ImageName/etc/modprobe.d/alsa-base.conf"

# bug fixes

# Zero out blocks for maximizing compression
dd if=/dev/zero of="images/$ImageName/tmp/zero.out" bs=4M
rm "images/$ImageName/tmp/zero.out"

# make sure that changes are actuall written to disk
sync
umount -fl "images/$ImageName/var/cache/apt/archives/" "images/$ImageName/*"
umount -fl "images/$ImageName/"
sync
#bzip2 -zk "images/$ImageName-root.image"
