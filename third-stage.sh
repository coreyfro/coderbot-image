#!/bin/bash
set -x

groups="adm,dialout"
user=pi

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true LC_ALL=C LANGUAGE=C LANG=C
cd /home/"$user"/build/PIGPIO/
make -j 10
make install
make clean
update-rc.d pigpiod defaults
update-rc.d coderbot defaults

mkdir -p /home/"$user"/build/opencv/build
cd /home/"$user"/build/opencv/build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D INSTALL_C_EXAMPLES=OFF -D INSTALL_PYTHON_EXAMPLES=OFF -D OPENCV_EXTRA_MODULES_PATH=/home/"$user"/build/opencv_contrib/modules -D BUILD_EXAMPLES=OFF ..
make -j 10
make install
make clean

exit
